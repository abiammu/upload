import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { ShowComponent } from './home/show/show.component';
import { ImageuploadComponent } from './image/imageupload/imageupload.component';
import { HashlistComponent } from './image/hashlist/hashlist.component';
import { AuthGuard } from './authguard/auth.guard';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'show',component:ShowComponent},
  {path:'imageupload',component:ImageuploadComponent,canActivate:[AuthGuard]},
  {path:'hashlist',component:HashlistComponent,canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
