import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  category: any;

 

  constructor(private http: HttpClient, private _router: Router) {}

  
  loggedIn() {
    return !!localStorage.getItem('email');
  }
  
  
}
