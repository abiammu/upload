import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {




getCookieValue={};
constructor(
 private _auth:AuthService,
  private router: Router
) { }
getlocalStorage(cookieName) {
  return JSON.parse(localStorage.getItem(cookieName));
}
canActivate(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): boolean {
    console.log("sdadssadS____________canActivate");
    
    if (!this._auth.loggedIn()) {
      console.log('asdasdasdsadsad_________login')
      this.router.navigateByUrl('');
      return false;
    }
    
    return true;
}
}