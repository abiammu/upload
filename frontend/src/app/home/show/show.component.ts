import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  view(){
    swal({
      text:"Please login to upload the Image",
      icon:"warning"
    })
  }
}
