import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { ShowComponent } from './home/show/show.component';
import { ImageuploadComponent } from './image/imageupload/imageupload.component';
import {AppHeaderModule} from '@coreui/angular';
import {HttpClientModule} from '@angular/common/http';
import {ConfirmEqualValidatorDirective} from './user/share/confirm-equal-validator.directive';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { ModalModule, BsModalService } from 'ngx-bootstrap';
import { HashlistComponent } from './image/hashlist/hashlist.component';
import {AuthGuard} from './authguard/auth.guard';
import {AuthService} from './auth.service';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    ShowComponent,
    ConfirmEqualValidatorDirective,
    ImageuploadComponent,
    HashlistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppHeaderModule,
    FormsModule,
    ModalModule.forRoot(),
    NgxPasswordToggleModule,
    HttpClientModule
  ],
  providers: [BsModalService,AuthGuard,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
