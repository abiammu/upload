import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-hashlist',
  templateUrl: './hashlist.component.html',
  styleUrls: ['./hashlist.component.scss']
})
export class HashlistComponent implements OnInit {
emailid:any;
hashobj:any=[];
  constructor(public router:Router,public http:HttpClient) { }

  ngOnInit() {
    this.emailid=localStorage.getItem('email')
    this.gethash();
  }
back(){
  this.router.navigate(['./imageupload'])
}
gethash(){
  var hashapi="http://localhost:3000/getfile?email="+ this.emailid;
  this.http.get(hashapi).subscribe(res=>{
    this.hashobj=res['payload'];
    console.log('ffff',this.hashobj);
  })
}
}
