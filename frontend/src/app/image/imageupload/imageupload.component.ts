import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';

@Component({
  selector: 'app-imageupload',
  templateUrl: './imageupload.component.html',
  styleUrls: ['./imageupload.component.scss']
})
export class ImageuploadComponent implements OnInit {
  selectedPath: File = null;
  emailid:any;
  ipfsdata:any={};
  constructor(private router:Router,private http:HttpClient) { }

  ngOnInit() {
    this.emailid=localStorage.getItem('email');
    this.ipfsdata={
      image:''
    }
  }
  logout(){ 
    localStorage.removeItem('email');
    this.router.navigate([''])
  }
  captureFile(event) {
    this.selectedPath = <File>event.target.files[0];
    console.log('this selected path', this.selectedPath);
}
submit(){
  const formData = new FormData();
  formData.append('image', this.selectedPath);
  var ipfsapi='http://localhost:3000/addfile?email=' + this.emailid;
  console.log('ffffffff',this.selectedPath);
  this.http.post(ipfsapi,formData).subscribe((res:any)=>{
    console.log(res.payload);
    swal({
      text:"Hash Created Successfully",
      icon:"success"
    })

})

}
go(){
  this.router.navigate(['./hashlist']);
}


}
