import { Component, OnInit,TemplateRef,ViewChild} from '@angular/core';
import { Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginData:any={};
  modalRef: BsModalRef;
  otpIsVerified: any;
  isVerified: any = '';
  @ViewChild('dummy') otpModal: TemplateRef<any>;

  constructor(public router:Router,private http:HttpClient,public modalService: BsModalService) { }

  ngOnInit() {
    this.loginData={
      email:'',
      password:'',
      otp:''
    }
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    console.log('this.modalRef:', typeof this.modalRef.hide)
  }
  closeModal() {
    if (typeof this.modalRef !== 'undefined') {
      this.modalRef.hide();
    }else{
      console.log('modalRef is not initialized.');
    }
  }

  otpverification() {
    var apiOtp = 'http://localhost:3000/verifyOtp?email=' + this.loginData.email;
    this.http.post(apiOtp, this.loginData).subscribe(
      data => {
        console.log(data);
        this.login();
      },
      error => {
        if (error.status == 409) {
          swal({
            text: "Incorrect Secret Key",
            icon: "error",
          });
        }

      }
    );

  }
  isUserVerified() {
    var apiUserVerified = 'http://localhost:3000/isUserVerified?email=' + this.loginData.email;
    this.http.get(apiUserVerified).subscribe(
      data => {
        this.otpIsVerified = data;

        this.isVerified = this.otpIsVerified.data.isVerified;

        console.log('Is Verified Func::', this.isVerified);
      }
    )
  }
  login(){
    var loginapi="http://localhost:3000/login";
    this.http.post(loginapi,this.loginData).subscribe(res=>{
      this.isUserVerified();
      console.log(res);
      localStorage.setItem('email',this.loginData.email)
      swal({
        text:"User Login Successfully",
        icon:"success"
      })
      this.router.navigate(['./imageupload']);
      this.closeModal();

},
error => {
  console.log('UI:', error)
  if (error.status == 402) {
     this.openModal(this.otpModal);
  }
  // if (error.status == 404) {
  //   swal({

  //     text: "User not found check the mail address",
  //     icon: "error",
  //   });
  // }
  if (error.status == 401) {
    swal({

      text: "Incorrect Password",
      icon: "error",
    });
  }

}
)


  }

}
