import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerData:any={};
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.registerData={
      email:'',
      username:'',
      password:'',
    }
    
  }
  register(){
    var registerapi="http://localhost:3000/userSignup";
    // localStorage.setItem('email',this.registerData.email)
    this.http.post(registerapi,this.registerData).subscribe(res=>{
      console.log(res);
      swal({
        text:"User Registered Successfully",
        icon:"success"
      })

})


}
}