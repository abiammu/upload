'use strict';
const multer = require('multer');

module.exports = function (app) {
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/ipfs/');
        },
        filename: (req, file, cb) => {
            cb(null,file.originalname);
        }
    });
    var image = multer({ storage: storage });
   
    var verifyOtp = require('../controllers/Email/verifyOtp');
    app.route('/verifyOtp').post(verifyOtp.verifyotp);

    var isUserVerified = require('../controllers/Email/isUserVerified');
    app.route('/isUserVerified').get(isUserVerified.isUserVerified);


    var userSignup = require('../controllers/user/userSignup');
    app.route('/userSignup').post(userSignup.userSignup);

    var usergetPlans = require('../controllers/user/Login');
    app.route('/login').post(usergetPlans.Login);

    var ipfsc= require('../controllers/user/ipfs');
    app.route('/addfile').post(image.single('image'),ipfsc.ipfscode);

    var files = require('../controllers/user/getAllfiles');
    app.route('/getfile').get(files.getAllfiles);
  
};
