'use strict';
/*
* Copyright IBM Corp All Rights Reserved
*
* SPDX-License-Identifier: Apache-2.0
*/
/*
 * Chaincode query
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');
var LocalStorage = require('node-localstorage').LocalStorage;

var verifyemailId = require('../../models/verification');

exports.Login = function (req, res) {
	var emailId = req.body.email;
	var password = req.body.password;
	var fabric_client = new Fabric_Client();

	// setup the fabric network
	var channel = fabric_client.newChannel('mychannel');
	var peer = fabric_client.newPeer('grpc://localhost:7051');
	channel.addPeer(peer);



	//
	var member_user = null;
	var store_path = path.join(os.homedir(), '.hfc-key-store');
	console.log('Store path:' + store_path);
	var tx_id = null;

	// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
	Fabric_Client.newDefaultKeyValueStore({
		path: store_path
	}).then((state_store) => {
		// assign the store to the fabric client
		fabric_client.setStateStore(state_store);
		var crypto_suite = Fabric_Client.newCryptoSuite();
		// use the same location for the state store (where the users' certificate are kept)
		// and the crypto store (where the users' keys are kept)
		var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
		crypto_suite.setCryptoKeyStore(crypto_store);
		fabric_client.setCryptoSuite(crypto_suite);

		// get the enrolled user from persistence, this user will sign all requests
		return fabric_client.getUserContext(emailId, true);
	}).then((user_from_store) => {
		if (user_from_store && user_from_store.isEnrolled()) {
			console.log('Successfully loaded emailId from persistence');
			member_user = user_from_store;
		} else {
			throw new Error('Failed to get emailId.... run registerUser.js');
		}

		// queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
		// queryAllCars chaincode function - requires no arguments , ex: args: [''],
		const request = {
			//targets : --- letting this default to the peers assigned to the channel
			chaincodeId: 'book',
			fcn: 'getPassword',
			args: [emailId]
		};

		// send the query proposal to the peer
		return channel.queryByChaincode(request);
	}).then((query_responses) => {
		console.log("Query has completed, checking results");
		// query_responses could have more than one  results if there multiple peers were used as targets
		if (query_responses && query_responses.length == 1) {
			if (query_responses[0] instanceof Error) {
				console.error("error from query = ", query_responses[0]);
			} else {
				console.log("Response is ", JSON.parse(query_responses[0].toString()));
				var pass = JSON.parse(query_responses[0].toString())

				verifyemailId.find({ emailId: req.body.email }).then(verify => {
					console.log(verify);
					if (verify[0].isVerified == true) {
						if (typeof localStorage === "undefined" || localStorage === null) {
							var localStorage = new LocalStorage('./secret');
						}
						localStorage.removeItem(emailId);


						if (pass[0].password == password) {

							return res.status(202).json({
								message: 'Login sucessfully',
								status: 202,

							});
						}
						else {
							return res.status(400).json({
								message: "Incorrect Password",
								status: 400,


							});
						}
					} else {
						res.status(402).json({
							message: 'EmailID is not verified yet',
							status: 402
						});
					}
				})
			}
		} else {
			console.log("No payloads were returned from query");
		}
	}).catch((err) => {
		console.error('Failed to query successfully :: ' + err);
	});
}