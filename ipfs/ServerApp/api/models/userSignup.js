var mongoose = require('mongoose');
var schema = mongoose.Schema;

var userSchema= new schema({
    userName:{type:String , required:true},
    emailId:{type:String , required:true},
    password:{type:String , required:true} 
});

module.exports = mongoose.model('UserSignup',userSchema);