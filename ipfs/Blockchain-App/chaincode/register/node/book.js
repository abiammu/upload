/*
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
*/

'use strict';
const shim = require('fabric-shim');
const util = require('util');

let Chaincode = class {

  // The Init method is called when the Smart Contract 'fabcar' is instantiated by the blockchain network
  // Best practice is to have any Ledger initialization in separate function -- see initLedger()
  async Init(stub) {
    console.info('=========== Instantiated fabcar chaincode ===========');
    return shim.success();
  }

  // The Invoke method is called as a result of an application request to run the Smart Contract
  // 'fabcar'. The calling application program has also specified the particular smart contract
  // function to be called, with arguments
  async Invoke(stub) {
    let ret = stub.getFunctionAndParameters();
    console.info(ret);

    let method = this[ret.fcn];
    if (!method) {
      console.error('no function of name:' + ret.fcn + ' found');
      throw new Error('Received unknown function ' + ret.fcn + ' invocation');
    }
    try {
      let payload = await method(stub, ret.params);
      return shim.success(payload);
    } catch (err) {
      console.log(err);
      return shim.error(err);
    }
  }

  async initLedger(stub, args) {

  }

  async userSignup(stub, args) {
    console.info('============= START : User Signup ===========');

    var user = {
      emailId: args[0],
      password: args[1],
      userName: args[2],
      secretkey: args[3]
    };
    console.log(user)
    await stub.putState(args[0], Buffer.from(JSON.stringify(user)));
    console.info('============= END : User Signup ===========');
  }

  async Ipfs(stub, args) {
    console.info('============= START : User Signup ===========');

    var users = {
      emailId: args[0],
      ipfsfile: args[1]
    };
    console.log(users)
    await stub.putState(args[1], Buffer.from(JSON.stringify(users)));
    console.info('============= END : User Signup ===========');
  }
  
  async getPassword(stub, args) {
    var emailId = args[0];

    let startKey = '';
    let endKey = '';

    let iterator = await stub.getStateByRange(startKey, endKey);

    let allResults = [];
  
    while (true) {
      let res = await iterator.next();
     
      if (res.value && res.value.value.toString()) {
        let jsonRes = {};
        jsonRes.Key = res.value.key;
        try {

          jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
        } catch (err) {
          console.log(err);
          jsonRes.Record = res.value.value.toString('utf8');
        }
        if (jsonRes.Record.emailId == emailId && jsonRes.Record.password) {

          allResults.push(jsonRes.Record);
        }
      }

      if (res.done) {
        console.log('end of data');
        await iterator.close();
        console.info(allResults);
        return Buffer.from(JSON.stringify(allResults));
      }

    }

  }


  async getAllFiles(stub, args) {
    var emailId = args[0];

    let startKey = '';
    let endKey = '';

    let iterator = await stub.getStateByRange(startKey, endKey);

    let allResults = [];
    while (true) {
      let res = await iterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};
        console.log(res.value.value.toString('utf8'));

        jsonRes.Key = res.value.key;
        try {
          jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));

        } catch (err) {
          console.log(err);
          jsonRes.Record = res.value.value.toString('utf8');
        }
        if (jsonRes.Record.emailId == emailId && jsonRes.Record.ipfsfile) {

          allResults.push(jsonRes.Record);
        }
      }
      if (res.done) {
        console.log('end of data');
        await iterator.close();
        console.info(allResults);
        return Buffer.from(JSON.stringify(allResults));
      }
    }
  }

}

shim.start(new Chaincode());
